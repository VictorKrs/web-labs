//Проверка логина
function CheckLog() {	
	var strLog = document.getElementsByName("Log")[0].value;
	var reg = /[^a-zA-Z0-9_-]/;
	var ch = reg.test(strLog);
	
	return strLog.length == 0 || ch;
}

//Проверка пароля
function CheckPass() {
	var strPass = document.getElementsByName("Pass")[0].value;
	var reg = /[^a-zA-Z0-9]/
	var ch = reg.test(strPass);
	
	if((ch) || (strPass.length < 6))
		return true;
	else
		return false;
}

//Проверка почты
function CheckMail() {
	var strMail = document.getElementsByName("Mail")[0].value;
	var ind_Dog = strMail.indexOf('@');
	var ind_Point = strMail.indexOf('.');
	var str1 = strMail.substring(0, ind_Dog);
	var str2 = strMail.substring(ind_Dog + 1, ind_Point);
	var str3 = strMail.substring(ind_Point + 1);
	
	return strMail.length == 0 || CheckMailFirst(str1) || CheckMailSecond(str2) || CheckMailThird(str3); 
}

//Проверка почты до @
function CheckMailFirst(str) {
	var reg = /[!-,./:-@\[-^`{-~]/;
	return str.length == 0 || reg.test(str);
}

//Проверка почты от @ до .
function CheckMailSecond(str) {
	var reg = /[!-,./:-@\[-^`{-~]/;
	return str.length == 0 || reg.test(str);
}

//Проверка почты после .
function CheckMailThird(str) {
	var reg = /[!-,./:-@\[-^`{-~]/;
	return str.length == 0 || reg.test(str);
}

//Проверка на совпадение паролей
function CheckPassConf() {
	var strPass = document.getElementsByName("Pass")[0].value;
	var strPassConf = document.getElementsByName("PassConf")[0].value;
	
	return strPassConf.length == 0 || strPass.localeCompare(strPassConf);
}

// Проверка на обязательные поля
function CheckRequired() {
	var strLog = document.getElementsByName("Log")[0].value;	
	var strPass = document.getElementsByName("Pass")[0].value;	
	var strPassConf = document.getElementsByName("PassConf")[0].value;	
	var strMail = document.getElementsByName("Mail")[0].value;
	
	return strLog.length == 0 || strPass.length == 0 || strPassConf.length == 0 || strPass.Mail == 0;
}

//Проверка всех полей
function CheckAll() {
	var str = "";
	
	if (CheckRequired())
	{
		str = "Необходимо ввести поля, помеченные *;";			
		Ins_Text(str, "ResTextError");
	}			
	
	if (CheckLog())
	{
		str = "В поле логин могут присутсвовать только буквы латинского алфавита, цифры, '-' и '_';";
		Ins_Text(str, "ResTextError");
		ChangeFont('log', "ErrorText");
		Blink($("#log"), 0);
	}		

	if (CheckPass())
	{
		str = "Пароль должен быть длиной не менее 6-ти символов и может содержать только буквы латинского алфавита и цифры;";
		Ins_Text(str, "ResTextError");
		ChangeFont('pass', "ErrorText");
		Blink($("#pass"), 1);
	}		
	
	if (CheckPassConf())
	{
		str = "Пароли не совпадают;";
		Ins_Text(str, "ResTextError");
		ChangeFont('pass_conf', "ErrorText");
		Blink($("#pass_conf"), 2);
	}		
	
	if (CheckMail())
	{
		str = "Неправильно введён e-mail;";
		Ins_Text(str, "ResTextError");
		ChangeFont('mail', "ErrorText");
		Blink($("#mail"), 3);
	}		
	
	if (str.length == 0)
	{
		str = "Регистрация прошла успешно!";
		Ins_Text(str, "ResTextSuccess");
	}		
}

//Проверка данных для регистрации
function CheckReg() {
	ChangeFont('log', "ContentText");
	ChangeFont('pass', "ContentText");
	ChangeFont('pass_conf', "ContentText");
	ChangeFont('mail', "ContentText");
	ClearText();
	CheckAll();
}

//Удаление пробелов
function DelSpace(name) {
	var elem = document.getElementsByName(name)[0];
	elem.value = elem.value.replace(/\s*/g, "");
}

//Изменение стиля шрифта
function ChangeFont(name, style) {
	var cell = document.getElementById(name);
	cell.className = style;
}

//Вставка текста
function Ins_Text(str, style) {
	var elem = document.createElement('p');
	elem.innerHTML = str; 
	elem.className = style;
	var res_text = document.getElementById('Text_Res');
	res_text.appendChild(elem); 
}

//Удаление текста сообщения
function ClearText() {
	var res_text = document.getElementById('Text_Res');
	var text_elements = document.getElementsByTagName('p');
	
	for (var i = 1; i < text_elements.length; )
		res_text.removeChild(text_elements[i]);
}

//Мигание текста
function Blink ($id, ind){
	$id.animate({opacity: "0.5"}, 500);
	$id.animate({opacity: "1"}, 500, function(){CheckAnimate($id, ind);});
}

//Проверка анимации
function CheckAnimate($id, ind) {
	var res = 0;
	
	switch (ind) {
		case 0:
			res = CheckLog();
		break;
		case 1:
			res = CheckPass();
		break;
		case 2:
			res = CheckPassConf();
		break;
		case 3:
			res = CheckMail();
		break;
		default:
			alert("FatalError");
		break;
	}
	
	if (res)
		Blink($id, ind);
	else 
	{
		$id.attr("class", "ContentText");
	}
}
